package com.runranking.runnerdelete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.sql2o.Connection;

import com.mack.sales.tools.ConnectionManager;
import com.mack.sales.tools.DBUtil;
import com.mackwilliams.race.models.Runner;

public class DeleteRunner {
	// Connection,Resultset , PrepStatement Declaration
	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet rsa = null;
	// Empty Runner object
	Runner r = null;

	public static void main(String[] args) {
		//System.out.println("in main");
		//DeleteRunner d = new DeleteRunner();
		//d.deleteRunner("100");
	}

	public void deleteRunner(String bib) {
		System.out.println("running");
		try (Connection con = ConnectionManager.getSql2o().open()) {

			
			// CallableStatement cStmt = con.prepareCall("{call delete_runner(?,
			// ?)}");

			String rBib = "100"; // r.getRunnerBIB();
			String query = "{ CALL delete_runner()?}"; // for stored proc taking
														// 2
														// parameters
			
			
			con.createQuery(query, true).addParameter(rBib, 1)
					// .addParameter("password", encryptedValue)
					.executeUpdate();
			// ResultSet rs = pstmt.executeQuery();

		} finally {
			DBUtil.close(rsa);
			DBUtil.close(pstmt);

		}

	}

}
